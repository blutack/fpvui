#!/usr/bin/env python

import sys, os
import pygtk, gtk, gobject
import pygst
pygst.require("0.10")
import gst
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import time

class GTK_Main:

	def __init__(self):
		window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		window.set_title("FPV Viewer")
		window.set_default_size(800,600)
#720, 576
		window.connect("destroy", gtk.main_quit, "WM destroy")
		vbox = gtk.VBox()
		window.add(vbox)
		self.movie_window = gtk.DrawingArea()
		vbox.add(self.movie_window)

		window.show_all()

		# Set up the gstreamer pipeline
		self.player = gst.parse_launch ('v4l2src ! ffmpegcolorspace ! rsvgoverlay name=overlay ! ffmpegcolorspace ! autovideosink')
		
		self.overlay = self.player.get_by_name('overlay')
		self.xml = open("drawing.svg").read()
		self.new_time = time.strftime("T %H:%M:%S")
		self.xml = self.xml.replace("T 00:00:00", self.new_time)

		bus = self.player.get_bus()
		bus.add_signal_watch()
		bus.enable_sync_message_emission()
		bus.connect("message", self.on_message)
		bus.connect("sync-message::element", self.on_sync_message)
		toggler = FullscreenToggler(window)
		window.connect_object('key-press-event', FullscreenToggler.toggle, toggler)
		self.player.set_state(gst.STATE_PLAYING)

	def update_overlay(self):
		tmp_time = time.strftime("T %H:%M:%S")
		self.xml = self.xml.replace(self.new_time, tmp_time)
		self.overlay.set_property("data", self.xml)
		self.new_time = tmp_time
		gobject.idle_add(self.update_overlay)

	def start_stop(self, w):
		if self.button.get_label() == "Start":
			self.button.set_label("Stop")
			
		else:
			self.player.set_state(gst.STATE_NULL)
			self.button.set_label("Start")

	def exit(self, widget, data=None):
		gtk.main_quit()

	def on_message(self, bus, message):
		t = message.type
		if t == gst.MESSAGE_EOS:
			self.player.set_state(gst.STATE_NULL)
			self.button.set_label("Start")
		elif t == gst.MESSAGE_ERROR:
			err, debug = message.parse_error()
			print "Error: %s" % err, debug
			self.player.set_state(gst.STATE_NULL)
			self.button.set_label("Start")

	def on_sync_message(self, bus, message):
		if message.structure is None:
			return
		message_name = message.structure.get_name()
		if message_name == "prepare-xwindow-id":
			# Assign the viewport
			imagesink = message.src
			imagesink.set_property("force-aspect-ratio", True)
			imagesink.set_xwindow_id(self.movie_window.window.xid)

class FullscreenToggler(object):

    	def __init__(self, window, keysym=gtk.keysyms.F11):
        	self.window = window
        	self.keysym = keysym
        	self.window_is_fullscreen = False
        	self.window.connect_object('window-state-event',
                                   FullscreenToggler.on_window_state_change,
                                   self)

    	def on_window_state_change(self, event):
        	self.window_is_fullscreen = bool(gtk.gdk.WINDOW_STATE_FULLSCREEN & event.new_window_state)

    	def toggle(self, event):
        	if event.keyval == self.keysym:
            		if self.window_is_fullscreen:
                		self.window.unfullscreen()
            		else:	
                		self.window.fullscreen()

foo = GTK_Main()
foo.update_overlay()
gtk.gdk.threads_init()
gtk.main()
